package br.com.investimento.controllers;

import br.com.investimento.models.DTOs.RespostaDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.services.InvestimentoService;
import br.com.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento registrarInvestimento(@RequestBody @Valid Investimento investimento){
        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);

        return investimentoObjeto;
    }

    @GetMapping
    public Iterable<Investimento> mostrarInvestimentos(){
        Iterable<Investimento> investimentos = investimentoService.buscarTodos();
        return  investimentos;
    }

    @PostMapping("/{id}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public RespostaDTO registrarSimulacao(@PathVariable Long id, @RequestBody @Valid Simulacao simulacao){

        try {
            RespostaDTO respostaDTO = simulacaoService.salvarSimulacao(id, simulacao);

            return respostaDTO;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }

    }


}
