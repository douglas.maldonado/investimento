package br.com.investimento.models.DTOs;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class RespostaDTO {

    private double rendimentoPorMes;
    private double montante;

    public RespostaDTO(){}

    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public double getMontante() {
        return montante;
    }

    public void setMontante(double montante) {
        BigDecimal bd = new BigDecimal(montante).setScale(2, RoundingMode.FLOOR);
        this.montante = bd.doubleValue();
    }
}
