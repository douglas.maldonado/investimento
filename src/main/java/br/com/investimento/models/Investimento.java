package br.com.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @Size(min = 5, max = 150, message = "O nome deve estar entre 5 e 150 caracteres")
    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser só espaços")
    private String nome;

    @DecimalMin(value = "0.1", message = "Redimento não pode ser zero")
    @Digits(integer = 6, fraction = 2, message = "Valor de rendimento formato invalido")
    private double rendimentoAoMes;

    public Investimento(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}


