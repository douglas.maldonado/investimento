package br.com.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(min = 5, max = 150, message = "O nome deve estar entre 5 e 150 caracteres")
    @NotNull(message = "Nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser só espaços")
    private String nomeInteressado;

    @Email(message = "O formato de Email não é válido")
    @NotNull(message = "Email não pode ser nulo")
    private String email;

    @NotNull(message = "Valor não pode ser nulo")
    private double valorAplicado;

    @NotNull(message = "Quantidade de meses não pode ser nulo")
    @Min(value = 1, message = "Periodo precisa ser maior que zero")
    private int quantidadeMeses;

    @ManyToOne
    private Investimento investimento;

    public Simulacao(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
