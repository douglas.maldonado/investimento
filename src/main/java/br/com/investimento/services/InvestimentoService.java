package br.com.investimento.services;


import br.com.investimento.models.Investimento;
import br.com.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento){

        Investimento investimentoObjeto = investimentoRepository.save(investimento);

        return investimentoObjeto;
    }

    public Iterable<Investimento> buscarTodos(){
        Iterable<Investimento> investimento = investimentoRepository.findAll();
        return  investimento;
    }

    public Investimento buscarPorId(Long id){
        Optional<Investimento> optionalInvestimento = investimentoRepository.findById(id);
        if (optionalInvestimento.isPresent()){
            return optionalInvestimento.get();
        }
        throw new RuntimeException("Produto de Investimento não encontrado");
    }




}
