package br.com.investimento.services;

import br.com.investimento.models.DTOs.RespostaDTO;
import br.com.investimento.models.Investimento;
import br.com.investimento.models.Simulacao;
import br.com.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    public RespostaDTO salvarSimulacao(Long id, Simulacao simulacao){

        Investimento investimento = investimentoService.buscarPorId(id);
        simulacao.setInvestimento(investimento);
        Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);

        RespostaDTO respostaDTO = calcularInvestimento(simulacao, investimento);

        return respostaDTO;
    }

    public RespostaDTO calcularInvestimento(Simulacao simulacao, Investimento investimento){
        RespostaDTO respostaDTO = new RespostaDTO();
        double montante = simulacao.getValorAplicado();

        for(int i=0; i < simulacao.getQuantidadeMeses(); i++){
            montante += montante * (investimento.getRendimentoAoMes()/100);
        }

        respostaDTO.setRendimentoPorMes(investimento.getRendimentoAoMes());
        respostaDTO.setMontante(montante);

        return respostaDTO;

    }


    public Iterable<Simulacao> buscarTodos(){
        Iterable<Simulacao> simulacao = simulacaoRepository.findAll();

        return simulacao;
    }




}
